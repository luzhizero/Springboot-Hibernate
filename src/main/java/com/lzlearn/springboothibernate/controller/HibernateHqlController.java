package com.lzlearn.springboothibernate.controller;

import com.lzlearn.springboothibernate.model.User;
import com.lzlearn.springboothibernate.service.HibernateHqlService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("hql")
public class HibernateHqlController {
    @Resource
    private HibernateHqlService hibernateHqlService;
    @RequestMapping("selectall")
    public Object selectAll(){
        List<User> users = hibernateHqlService.selectAll();
        return users;
    }

    @RequestMapping("selectlike")
    public Object selectLike(String name){
        List<User> users = hibernateHqlService.selectLike(name);
        return  users;
    }

    @RequestMapping("selectcondition")
    public Object selectCondition(String name,Integer pageIndex ,Integer pageCount){
        List<User> users = hibernateHqlService.selectCondition(name,pageIndex,pageCount);
        return  users;
    }

    @RequestMapping("update")
    public Object updateUser(User user){
        boolean result = hibernateHqlService.updateUser(user);
        return result;
    }

    @RequestMapping("selectAndUpdate")
    public Object selectAndUpdate(User user){
        User user1 = hibernateHqlService.selectAndUpdate(user);
        return user1;
    }
}
