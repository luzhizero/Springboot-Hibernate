package com.lzlearn.springboothibernate.controller;

import com.lzlearn.springboothibernate.model.User;
import com.lzlearn.springboothibernate.service.OldcriteriaService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("cri")
public class OldcriteriaController {
    @Resource
    private OldcriteriaService oldcriteriaService;

    @RequestMapping("cricondition")
    public Object criCondition(String name,Integer pageIndex ,Integer pageCount){
        List<User> users = oldcriteriaService.criCondition(name,pageIndex,pageCount);
        return users;
    }
}
