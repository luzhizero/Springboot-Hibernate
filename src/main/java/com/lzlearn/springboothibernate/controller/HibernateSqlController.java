package com.lzlearn.springboothibernate.controller;

import com.lzlearn.springboothibernate.model.User;
import com.lzlearn.springboothibernate.service.HibernateSqlService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("sql")
public class HibernateSqlController {
    @Resource
    private HibernateSqlService hibernateSqlService;

    @RequestMapping("selectCondition")
    public Object selectAll(String name,Integer pageIndex ,Integer pageCount){
        List<User> users = hibernateSqlService.selectCondition(name,pageIndex,pageCount);
        return users;
    }

    @RequestMapping("update")
    public Object updateUser(User user){
        boolean result = hibernateSqlService.updateUser(user);
        return result;
    }
}
