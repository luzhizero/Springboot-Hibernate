package com.lzlearn.springboothibernate.service;

import com.lzlearn.springboothibernate.model.User;

import java.util.List;

public interface HibernateSqlService {
    List<User> selectCondition(String name,Integer pageIndex ,Integer pageCount);

    boolean updateUser(User user);
}
