package com.lzlearn.springboothibernate.service;

import com.lzlearn.springboothibernate.model.User;

import java.util.List;

public interface HibernateHqlService {
    List<User> selectAll();

    List<User> selectLike(String name);

    List<User> selectCondition(String name, Integer pageIndex, Integer pageCount);

    boolean updateUser(User user);

    User selectAndUpdate(User user);
}
