package com.lzlearn.springboothibernate.service;

import com.lzlearn.springboothibernate.model.User;

import java.util.List;

public interface OldcriteriaService {
    List<User> criCondition(String name, Integer pageIndex, Integer pageCount);
}
