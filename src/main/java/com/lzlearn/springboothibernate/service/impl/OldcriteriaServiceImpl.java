package com.lzlearn.springboothibernate.service.impl;

import com.lzlearn.springboothibernate.dao.OldcriteriaDao;
import com.lzlearn.springboothibernate.model.User;
import com.lzlearn.springboothibernate.service.OldcriteriaService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class OldcriteriaServiceImpl implements OldcriteriaService {
    @Resource
    private OldcriteriaDao oldcriteriaDao;

    @Override
    public List<User> criCondition(String name, Integer pageIndex, Integer pageCount) {
        List<User> users = oldcriteriaDao.criCondition(name,pageIndex,pageCount);
        return users;
    }
}
