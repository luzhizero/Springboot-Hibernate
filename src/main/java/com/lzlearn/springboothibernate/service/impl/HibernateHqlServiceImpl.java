package com.lzlearn.springboothibernate.service.impl;

import com.lzlearn.springboothibernate.dao.HibernateHqlDao;
import com.lzlearn.springboothibernate.dao.MyHibernateHqlDao;
import com.lzlearn.springboothibernate.model.User;
import com.lzlearn.springboothibernate.service.HibernateHqlService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class HibernateHqlServiceImpl implements HibernateHqlService {
    @Resource
    private HibernateHqlDao hibernateHqlDao;
    @Resource
    private MyHibernateHqlDao myHibernateHqlDao;
    @Override
    public List<User> selectAll() {
        List<User> users = hibernateHqlDao.findAll();
        return users;
    }

    @Override
    public List<User> selectLike(String name) {
        List<User> users = hibernateHqlDao.selectLike(name);
        return users;
    }

    @Override
    public List<User> selectCondition(String name, Integer pageIndex, Integer pageCount) {
        List<User> users = myHibernateHqlDao.selectCondition(name,pageIndex,pageCount);
        return users;
    }

    @Override
    public boolean updateUser(User user) {
        boolean result = myHibernateHqlDao.updateUser(user);
        return result;
    }

    @Override
    public User selectAndUpdate(User user) {
        User user1  = myHibernateHqlDao.selectAndUpdate(user);
        return user1;
    }
}
