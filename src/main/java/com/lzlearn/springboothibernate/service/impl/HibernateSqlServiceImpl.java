package com.lzlearn.springboothibernate.service.impl;

import com.lzlearn.springboothibernate.dao.MyHibernateSqlDao;
import com.lzlearn.springboothibernate.model.User;
import com.lzlearn.springboothibernate.service.HibernateSqlService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class HibernateSqlServiceImpl implements HibernateSqlService {
    @Resource
    private MyHibernateSqlDao myHibernateSqlDao;

    @Override
    public List<User> selectCondition(String name,Integer pageIndex ,Integer pageCount) {
        List<User> users = myHibernateSqlDao.selectCondition(name,pageIndex,pageCount);
        return users;
    }

    @Override
    public boolean updateUser(User user) {
        boolean result = myHibernateSqlDao.updateUser(user);
        return result;
    }
}
