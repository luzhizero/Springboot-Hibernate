package com.lzlearn.springboothibernate;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.orm.jpa.vendor.HibernateJpaSessionFactoryBean;

import javax.persistence.EntityManagerFactory;

@SpringBootApplication
public class SpringbootHibernateApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootHibernateApplication.class, args);
    }


}
