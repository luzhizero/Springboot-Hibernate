package com.lzlearn.springboothibernate.dao;

import com.lzlearn.springboothibernate.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface HibernateHqlDao extends JpaRepository<User,Long> {

    @Query("FROM User WHERE name LIKE %:name%")
    List<User> selectLike(String name);


}
