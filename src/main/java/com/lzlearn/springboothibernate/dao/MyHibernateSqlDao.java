package com.lzlearn.springboothibernate.dao;

import com.lzlearn.springboothibernate.model.User;

import java.util.List;

public interface MyHibernateSqlDao {
    List<User> selectCondition(String name,Integer pageIndex ,Integer pageCount);

    boolean updateUser(User user);
}
