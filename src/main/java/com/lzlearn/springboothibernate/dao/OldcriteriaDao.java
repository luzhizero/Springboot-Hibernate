package com.lzlearn.springboothibernate.dao;

import com.lzlearn.springboothibernate.model.User;

import java.util.List;

public interface OldcriteriaDao {
    List<User> criCondition(String name, Integer pageIndex, Integer pageCount);
}
